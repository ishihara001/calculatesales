package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	// 支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";

	// 支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";

	// 商品別集計ファイル名
	private static final String FILE_NAME_COMMODITY_LST = "commodity.lst";

	// 商品別集計ファイル名
	private static final String FILE_NAME_COMMODITY_OUT = "commodity.out";

	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	private static final String FILE_NOT_EXIST = "支店定義ファイルが存在しません";
	private static final String FILE_NOT_COMMODITY = "商品定義ファイルが存在しません";
	private static final String FILE_IS_INVALID = "のフォーマットが不正です";
	private static final String BRANCH_CODE_INVALID = "の支店コードが不正です";
	private static final String COMMODITY_CODE_INVALID = "の商品コードが不正です";
	private static final String FILE_NOT_SERTAL_NUMBER = "売上ファイル名が連番になっていません";
	private static final String NUMBER_OF_INVALID = "合計金額が10桁を超えました";

	private static final String REGULAR_EXPRESSION_BRANCH = "^[0-9]{3}$";
	private static final String REGULAR_EXPRESSION_COMMODITY = "^[1A-Za-z0-9]{8}$";
	private static final String REGULAR_EXPRESSION_FILENAME_RCD = "^[0-9]{8}+.rcd$";
	private static final String REGULAR_EXPRESSION_FILENAME_NUMERAL = "^[0-9]+$";

	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数
	 */
	public static void main(String[] args) {
		// コマンドライン引数が渡されているか確認（エラー処理3-1）
		if (args.length != 1) {
			System.out.println(UNKNOWN_ERROR);
		}
		// 支店コードと支店名を保持するMap
		Map<String, String> branchNames = new HashMap<>();
		// 支店コードと売上金額を保持するMap
		Map<String, Long> branchSales = new HashMap<>();
		// 商品コードと商品名を保持するMap
		Map<String, String> commodityNames = new HashMap<>();
		// 商品名と売上金額を保持するMap
		Map<String, Long> commoditySales = new HashMap<>();

		// 支店定義ファイル読み込み処理
		if (!readFile(args[0], FILE_NAME_BRANCH_LST, REGULAR_EXPRESSION_BRANCH, FILE_NOT_EXIST, "支店定義ファイル",
				branchNames, branchSales)) {
			return;
		}

		// 商品定義ファイル読み込み処理

		if (!readFile(args[0], FILE_NAME_COMMODITY_LST, REGULAR_EXPRESSION_COMMODITY, "商品定義ファイル",
				FILE_NOT_COMMODITY, commodityNames, commoditySales)) {
			return;
		}

		// ※ここから集計処理を作成してください。(処理内容2-1、2-2)--------------------------------------------------
		// 売上ファイルの習得
		// (処理内容2-1)-------------------------------------------------------------------------
		File[] files = new File(args[0]).listFiles(); // コマンドライン引数args[0]から獲得filesに格納

		// ファイルかつ数字8桁.rcdなのか判定してリストに格納----------------------------------------------------------
		List<File> rcdFiles = new ArrayList<>(); // リストrcdFile(ファイルを格納)を用意

		for (int i = 0; i < files.length; i++) { // failesに格納されてる数だけ繰り返し
			String fileName = files[i].getName(); // ファイル名の獲得
			if (files[i].isFile() && fileName.matches(REGULAR_EXPRESSION_FILENAME_RCD)) { // ファイルかつ数字8桁.rcdなのか判定
				rcdFiles.add(files[i]); // リストrcdFile(ファイルを格納)に格納
			}
		}
		// ファイルかつ数字8桁.rcdなのか判定してリストに格納終了------------------------------------------------------
		// 売上ファイル名の連番チエック（エラー処理2-1開始）----------------------------------------------------------
		Collections.sort(rcdFiles); // リストrcdFile(ファイルを格納)をソート
		for (int j = 0; j < rcdFiles.size() - 1; j++) {// リストrcdFile(ファイルを格納)の数を呼び出して繰り返し
			// リストrcdFile(ファイルを格納)から呼び出し,ファイル名の獲得
			int former = Integer.parseInt(((rcdFiles.get(j)).getName()).substring(0, 8)); // substring(0,
																							// 8)で指定した文字列を切り出し
			int latter = Integer.parseInt(((rcdFiles.get(j + 1)).getName()).substring(0, 8)); // pareIntで型変更

			if ((latter - former) != 1) {
				System.out.println(FILE_NOT_SERTAL_NUMBER);
				return;
			}
			// ------------------------F-----------(エラー処理2-1終了)-------------------------------------------------

		}
		// 売上ファイルの読み込み、支店コード、売上額を抽出(処理内容2-2)-----------------------------------------
		BufferedReader br = null; // br変数を宣言

		for (int i = 0; i < rcdFiles.size(); i++) { // リストrcdFile(ファイル格納)の数を呼び出して繰り返し
			try {
				ArrayList<String> values = new ArrayList<String>();// リストvalues(店番号と売上金額)を格納
				File fileName = rcdFiles.get(i); // リストrcdFile(ファイル格納)からファイルを呼び出し代入
				FileReader fr = new FileReader(fileName);
				br = new BufferedReader(fr); // brに読み込み待機中のファイル格納

				String line = null; // line変数を宣言

				while ((line = br.readLine()) != null) { // brから一行ずつ読み込む
					values.add(line); // リストvalues(店番号と売上金額)を格納
				}
				// 売上ファイルの中身が3行でなかった場合(エラー処理2-4開始)------------------------------------------
				if (values.size() != 3) {
					String file = files[i].getName();
					System.out.println(file + FILE_IS_INVALID);
					return;
				}
				// --------------------------------------(エラー処理2-4終了)-----------------------------------------

				String store = values.get(0); // 店舗コードをstoreに格納

				// 売上ファイルの支店コードが支店定義ファイルに該当しなかった場合（エラー処理2-3)--------
				if (!branchSales.containsKey(store)) {
					String file = files[i].getName();
					System.out.println(file + BRANCH_CODE_INVALID);
					return;
				}
				// -----------------------------------------(エラー処理2-3終了)-----------------------------

				String code = values.get(1);// 商品コードをstoreに格納

				// 売上ファイルの商品コードが商品定義ファイルに該当しなかった場合(エラー処理2-4)---------------------
				if (!commodityNames.containsKey(code)) {
					String file = files[i].getName();
					System.out.println(file + COMMODITY_CODE_INVALID);
					return;
				}

				String sale = values.get(2); // 売上金額をsalesに格納
				// ファイルの売上金額で数字がなかった場合の表示(エラー処理3-2)--------------------------------
				if (!sale.matches(REGULAR_EXPRESSION_FILENAME_NUMERAL)) {
					System.out.println(UNKNOWN_ERROR);
					return;
				}
				// -----------------------------------------(エラー処理3-2終了)---------------------------------
				Long fileSale = Long.parseLong(values.get(2)); // 売上金額の型の変更
				long storeTotal = branchSales.get(store) + fileSale; // 抽出した売上金額を加算する
				long itemTotal = commoditySales.get(code) + fileSale;
				// 売上金額の合計額のエラー処理（エラー処理2-2)--------------------------------------------------
				if ((storeTotal >= 10000000000L) || (itemTotal >= 10000000000L)) {
					System.out.println(NUMBER_OF_INVALID);
					return;
				}
				branchSales.put(store, storeTotal); // マップへの格納
				commoditySales.put(code, itemTotal);// マップへの格納
				// ---------------------------------(エラー処理2-2終了)-------------------------------------------
			} catch (IOException e) {
				System.out.println(UNKNOWN_ERROR);
				return;
			} finally {
				if (br != null) {
					try {
						br.close();

					} catch (IOException e) {
						System.out.println(UNKNOWN_ERROR);
						return;
					}
				}
			}
		}
		// ----------------------------------------(処理内容2-1、2-2終了)----------------------------------------
		// 支店別集計ファイル書き込み処理
		if (!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)) {
			return;
		}

		// 商品名別集計ファイル書き込み処理
		if (!writeFile(args[0], FILE_NAME_COMMODITY_OUT, commodityNames, commoditySales)) {
			return;
		}

	}

	/**
	 * 支店定義ファイル読み込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @param 商品コードと商品名を保持するMap
	 * @param 商品コードと売上金額を保持するMap
	 * @return 読み込み可否
	 */
	private static boolean readFile(String path, String fileName, String judgment, String errorFile, String errorFormat,
			Map<String, String> names, Map<String, Long> sales) {
		BufferedReader br = null;

		try {
			File files = new File(path, fileName);
			// ファイルの存在チエック(エラー処理1-1開始)-------------------------------------
			if (!files.exists()) {
				System.out.println(errorFile);
				return false;
			}
			// -----------------------(エラー処理1-1終了)--------------------------------------------
			FileReader fr = new FileReader(files);// ファイルの読み込み開始(1-2開始)------------------------
			br = new BufferedReader(fr);

			String line;
			// 一行ずつ読み込む
			while ((line = br.readLine()) != null) {

				String[] items = line.split(",");

				// ファイルのフォーマットチエック（エラー処理1-2開始)----------------------------------------
				if ((items.length != 2) || (!items[0].matches(judgment))) {
					System.out.println(errorFormat + FILE_IS_INVALID);
					return false;
				}
				// ------------------------(エラー処理1-2終了)--------------------------------------

				names.put(items[0], items[1]); // 支店別集計ファイルをマップに保存
				sales.put(items[0], 0L); // 売上集計ファイルの初期値をマップに格納
			}

		} catch (IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if (br != null) {
				try {
					// ファイルを閉じる
					br.close();
				} catch (IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

	// ---------------------------------------------ファイル読み込み終了(1-2)---------------------------------
	/**
	 * 支店別集計ファイル書き込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 書き込み可否
	 */
	private static boolean writeFile(String path, String fileName, Map<String, String> names,
			Map<String, Long> sales) {
		// ※ここに書き込み処理を作成してください。(処理内容3-1)-----------------------------------------------
		BufferedWriter bw = null;

		try {
			File files = new File(path, fileName); // branch.outファイルの作成しfileに格納
			FileWriter fw = new FileWriter(files);
			bw = new BufferedWriter(fw); // bwに書き込み待機中のファイル格納

			for (String key : names.keySet()) { // keyの数だけ繰り返す
				// 売上の書き込み
				bw.write(key + "," + names.get(key) + "," + sales.get(key));
				bw.newLine();
			}

		} catch (IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if (bw != null) {
				try {
					// ファイルを閉じる
					bw.close();
				} catch (IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}
	// -----------------------------------------------(3-1終了)-------------------------------------------
}
